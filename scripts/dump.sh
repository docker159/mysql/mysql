
######
##Dump mysql DATABAS1:
######
  mysqldump -u root -p DATABAS1 > /home/m.peshko/dumpdb/DATABAS1.sql

######
##Copy mysql dump to another server:
######
  scp dumpdb.sql m.peshko@anither.server

######
##Создаем базу данных и пользователя в mysql:
######
  CREATE SCHEMA `DATABAS1` CHARACTER SET utf8 COLLATE utf8_general_ci;
  CREATE USER 'USERDATABAS1'@'%' IDENTIFIED BY 'wery_secret_passowrd';
  ALTER USER 'USERDATABAS1'@'%' PASSWORD EXPIRE NEVER;
  GRANT ALL ON `DATABAS1`.* TO `USERDATABAS1`@'%';


######
##Разворачиваем dump DATABAS1:
######
  mysql -u root -p DATABAS1<DATABAS1.sql
