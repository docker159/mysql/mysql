######
##Создаем базу данных и пользователя в mysql:
######
  CREATE SCHEMA `simpsons` CHARACTER SET utf8 COLLATE utf8_general_ci;
  CREATE USER 'homer'@'%' IDENTIFIED BY 'simpson';
  ALTER USER 'homer'@'%' PASSWORD EXPIRE NEVER;
  GRANT ALL ON `simpsons`.* TO `homer`@'%';
  FLUSH PRIVILEGES
######
##Создаем новую таблицу
######
  CREATE TABLE city (
    `id` INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20), mayor VARCHAR(20),
    population SMALLINT(20),
    religion VARCHAR(20),
    PRIMARY KEY (`id`)
    );

  CREATE TABLE home (
    `id` INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20), mayor VARCHAR(20),
    population SMALLINT(20),
    religion VARCHAR(20),
    PRIMARY KEY (`id`)
    );

CREATE TABLE Currency (
  `currencyid` INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(32) NOT NULL,
  symbol VARCHAR(32) NOT NULL,
  after tinyint(1),
  space tinyint(1),
  deleted tinyint(1) NOT NULL,
  PRIMARY KEY (`currencyid`)
  );


  CREATE TABLE `Rule` (
    `pid` varchar(36) NOT NULL,
    `ruleid` int NOT NULL,
    `name` varchar(64) NOT NULL,
    PRIMARY KEY (`pid`,`ruleid`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

  CREATE USER 'debezium'@'%' IDENTIFIED BY '12345678';
  GRANT SELECT, RELOAD, SHOW DATABASES, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'debezium';


######
##Вставка данных в таблицу
######
  INSERT INTO city (name, mayor, population, religion) VALUES ('A', 'A', 1, 'A');
  INSERT INTO home (name, mayor, population, religion) VALUES ('A', 'A', 1, 'A');


  INSERT INTO Currency (currencyid, name, symbol) VALUES (14012, 'Mpp', 'q');
  INSERT INTO Currency (pid, ruleid, name) VALUES ("1", 1, '1');
